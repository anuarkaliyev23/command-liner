package org.bitbucket.anuarkaliyev23.command.liner.command

import kotlin.reflect.KClass
import org.bitbucket.anuarkaliyev23.command.liner.annotations.*

/**
 * > Implementations must be able to find class annotated with [CLICommand] among other similar classes
 * */
interface CommandRegistry {
    /**
     * Find class by [CLICommand.key].
     *
     * @param key - [CLICommand.key] parameter of [CLICommand] with which class is annotated with
     * @return class annotated with [CLICommand]
     * */
    fun commandOf(key: String): KClass<*>

    /**
     * Get All classes in registry
     * @return all classes this registry could find
     * */
    fun commands(): Set<KClass<*>>
}