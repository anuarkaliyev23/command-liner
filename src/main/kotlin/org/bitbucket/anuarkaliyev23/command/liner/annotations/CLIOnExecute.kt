package org.bitbucket.anuarkaliyev23.command.liner.annotations

/**
 * >Main method of the class annotated with [CLICommand] must be annotated with this annotation.
 * >It represents method which will be called on command's execution
 * */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class CLIOnExecute