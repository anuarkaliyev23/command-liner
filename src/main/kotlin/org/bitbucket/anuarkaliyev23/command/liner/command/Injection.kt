package org.bitbucket.anuarkaliyev23.command.liner.command

import kotlin.reflect.KMutableProperty1

interface Injection<T, R> {
    val field: KMutableProperty1<T, R>
    val fieldValue: R

    fun inject(instance: T) {
        field.set(instance, fieldValue)
    }
}