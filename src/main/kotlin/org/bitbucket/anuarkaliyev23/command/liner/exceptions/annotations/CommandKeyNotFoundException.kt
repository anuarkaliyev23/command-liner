package org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations

import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException

class CommandKeyNotFoundException(
        val key: String
): CLIException("Have not found command with key = [$key]")