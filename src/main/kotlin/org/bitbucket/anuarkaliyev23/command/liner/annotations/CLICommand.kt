package org.bitbucket.anuarkaliyev23.command.liner.annotations

/**
 * Class must be annotated with this annotation if it represents command in command line interface.
 *
 * >e.g.
 * >>`ls` - standard Unix command to list all files
 * >>`mkdir` - standard Unix command to create new folder
 * >>`git init` - command to create empty git repository
 * */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class CLICommand(
        /**
         * Must be unique to identify command along list of others
         *
         * e.g.
         * >`ls`
         * >`mkdir`
         * >`git init`
         * */
        val key: String,

        /**
         * Explanation of a command
         *
         * e.g.
         * >`List all files`,
         * >`Create new folder`,
         * >`Initialize empty git repository`
         * */
        val helpMessage: String
)