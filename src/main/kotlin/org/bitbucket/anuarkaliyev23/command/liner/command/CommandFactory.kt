package org.bitbucket.anuarkaliyev23.command.liner.command

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIArgument
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOptional
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.MandatoryArgumentKeyIsOmittedException
import org.bitbucket.anuarkaliyev23.command.liner.extensions.isAnnotatedWith
import org.bitbucket.anuarkaliyev23.command.liner.extensions.logger
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties

/**
 * Factory pattern for any class that is annotated with [org.bitbucket.anuarkaliyev23.command.liner.annotations.CLICommand]
 * */
interface CommandFactory {
    val injections: Set<Injection<*, *>>

    fun<T: Any> instantiateAndInject(command: KClass<T>, args: Map<String, String>): T {
        val instance = instantiate(command, args)
        inject(instance, command)

        return instance
    }

    fun<T : Any> instantiate(command: KClass<T>, args: Map<String, String>): T

    fun<T: Any> inject(instance: T, instanceClass: KClass<T>) {
        this.logger().debug("Starting to inject {$injections}")
        injections.filterIsInstance<Injection<T, *>>().forEach {
            if (instanceClass.memberProperties.contains(it.field)) {
                this.logger().debug("Injecting {$it} into {$instance}")
                it.inject(instance)
            }
        }
    }

    fun checkMandatoryProperties(command: KClass<*>, args: Map<String, String>) {
        val mandatoryProperties = command.memberProperties.filter { it.isAnnotatedWith<CLIArgument>() && !it.isAnnotatedWith<CLIOptional>() }
        val annotations = mandatoryProperties.mapNotNull { it.findAnnotation<CLIArgument>() }

        annotations.forEach { annotation ->
            if (!args.keys.contains(annotation.key))
                throw MandatoryArgumentKeyIsOmittedException(annotation.key, command)
        }
    }


}