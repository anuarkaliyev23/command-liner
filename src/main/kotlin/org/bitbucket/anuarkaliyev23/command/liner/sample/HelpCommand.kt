package org.bitbucket.anuarkaliyev23.command.liner.sample

import org.bitbucket.anuarkaliyev23.command.liner.annotations.*
import org.bitbucket.anuarkaliyev23.command.liner.command.CommandRegistry
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations.ClassNotAnnotatedAsCommandException
import org.bitbucket.anuarkaliyev23.command.liner.extensions.annotatedProperties
import org.bitbucket.anuarkaliyev23.command.liner.sample.HelpCommand.Companion.COMMAND_KEY
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.full.findAnnotation

@CLICommand(key = COMMAND_KEY, helpMessage = "Help message")
class HelpCommand {
    @CLIInjection(INJECTION_REGISTRY)
    lateinit var registry: CommandRegistry

    @CLIOnExecute
    fun execute(): String {
        val parsed = registry.commands().map { parseCommand(it) }
        val folded = parsed.foldRight("", {s, acc -> acc + s + "\n"})
        val result = """
            |Welcome to help menu.
            |
            |Commands: 
            |$folded
        """.trimMargin("|")
        return result
    }


    private fun parseCommand(command: KClass<*>): String {
        val annotation = command.findAnnotation<CLICommand>() ?: throw ClassNotAnnotatedAsCommandException(command)
        val arguments = command.annotatedProperties<CLIArgument>()
        val argumentStrings = arguments.map { parseArgument(it)}.foldRight("", {s, acc -> s + "\n" + acc })
        return """
            |${annotation.key}  -   ${annotation.helpMessage}
            |arguments:
            |$argumentStrings
        """.trimMargin();
    }
    private fun parseArgument(property: KProperty<*>): String{
        val annotation = property.findAnnotation<CLIArgument>() ?: throw CLIException("Argument not annotated")
        val isOptionalString = if (property.findAnnotation<CLIOptional>() != null) "Mandatory" else "Optional"
        val parserMethodName= annotation.parserMethodName
        val parserMethodString = if (parserMethodName.isEmpty()) "" else "[$parserMethodName]"

        return "($isOptionalString) ${annotation.key}\t-\t${annotation.helpMessage} $parserMethodString"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HelpCommand

        if (registry != other.registry) return false

        return true
    }

    override fun hashCode(): Int {
        return registry.hashCode()
    }

    override fun toString(): String {
        return "HelpCommand"
    }


    companion object {
        const val COMMAND_KEY = "help"
        const val INJECTION_REGISTRY = "registry"
    }
}