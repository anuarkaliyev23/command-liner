package org.bitbucket.anuarkaliyev23.command.liner.exceptions

import kotlin.reflect.KClass

class MandatoryArgumentKeyIsOmittedException(
        val key: String,
        val kClass: KClass<*>
): CLIException("Mandatory argument key = [$key] is omitted in class=[$kClass]") {
}