package org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations

import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException
import kotlin.reflect.KClass

open class ClassNotAnnotatedException(
        val kClass: KClass<*>,
        val supposedAnnotation: KClass<out Annotation>
) : CLIException("Necessary annotation = [$supposedAnnotation] is missing in class = [$kClass] or it's members")