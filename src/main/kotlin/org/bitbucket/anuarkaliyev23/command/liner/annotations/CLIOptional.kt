package org.bitbucket.anuarkaliyev23.command.liner.annotations

/**
 * Annotation intended to be used with [CLIArgument]. Marks optional (nullable, not mandatory) arguments.
 * */
@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class CLIOptional