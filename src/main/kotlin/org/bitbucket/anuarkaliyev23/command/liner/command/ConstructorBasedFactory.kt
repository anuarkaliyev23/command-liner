package org.bitbucket.anuarkaliyev23.command.liner.command

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIArgument
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIConstructor
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIParameter
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException
import org.bitbucket.anuarkaliyev23.command.liner.extensions.isAnnotatedWith
import org.bitbucket.anuarkaliyev23.command.liner.extensions.simpleName
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.*

class ConstructorBasedFactory(
        override val injections: Set<Injection<*, *>> = emptySet()
) : CommandFactory {
    override fun <T : Any> instantiate(command: KClass<T>, args: Map<String, String>): T {
        checkMandatoryProperties(command, args)
        val classConstructor = command.constructors.firstOrNull { it.isAnnotatedWith<CLIConstructor>() } ?: throw CLIException("No constructor annotated with [${CLIConstructor::class.simpleName}]")
        val parameters = classConstructor.parameters.sortedBy { it.index }
        val parameterValues = parameters.map { parameter -> parameterValue(command, parameter, args) }

        val instance = classConstructor.call(*parameterValues.toTypedArray())
        return instance
    }

    private fun parameterValue(command: KClass<*>, parameter: KParameter, args: Map<String, String>): Any? {
        val parameterAnnotation = parameter.annotations.filterIsInstance<CLIParameter>().first()
        val key = parameterAnnotation.key
        val parameterValue = args[key]

        val property = command.memberProperties.first { it.findAnnotation<CLIArgument>()?.key == key }
        val argumentAnnotation = property.findAnnotation<CLIArgument>()!!
        val parserMethodName = argumentAnnotation.parserMethodName

        if (parameterValue == null) {
            return null
        }

        return if (parserMethodName.isEmpty()) {
            parameterValue
        } else {
            val method = command.companionObject?.memberFunctions?.find { it.simpleName() == parserMethodName } ?: throw CLIException("No parser method with name = {$parserMethodName} found")
            val parsedValue = method.call(command.companionObjectInstance, parameterValue)
            parsedValue
        }
    }
}