package org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOnExecute
import kotlin.reflect.KClass

class CommandHasNoExecutorException(
        kClass: KClass<*>
) : ClassNotAnnotatedException(kClass, CLIOnExecute::class)