package org.bitbucket.anuarkaliyev23.command.liner.command

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLICommand
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOnExecute
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations.*
import org.bitbucket.anuarkaliyev23.command.liner.extensions.annotatedFunctions
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

class ImmutableCommandRegistry(val commands: Set<KClass<*>>): CommandRegistry {
    init {
        commands.forEach { checkCommand(it) }
        checkForDuplicates(commands)
    }

    private fun checkForDuplicates(commands: Set<KClass<*>>) {
        val classAnnotations = commands.mapNotNull { it.findAnnotation<CLICommand>() }
        val keys = classAnnotations.map { it.key }.toSet()
        if (classAnnotations.size != keys.size) {
            throw DuplicateKeyException(commands)
        }
    }

    private fun checkCommand(command: KClass<*>) {
        if (command.findAnnotation<CLICommand>() == null)
            throw ClassNotAnnotatedAsCommandException(command)

        val annotatedMethodsList = command.annotatedFunctions<CLIOnExecute>()

        if (annotatedMethodsList.isEmpty())
            throw CommandHasNoExecutorException(command)
        else if (annotatedMethodsList.size > 1)
            throw CommandMultipleExecutorException(command)
    }


    override fun commandOf(key: String): KClass<*> {
        commands.forEach {command ->
            val annotation = command.findAnnotation<CLICommand>()!!
            if (annotation.key == key)
                return command
        }
        throw CommandKeyNotFoundException(key)
    }

    override fun commands(): Set<KClass<*>> = commands

    override fun toString(): String {
        return "ImmutableCommandRegistry(commands=$commands)"
    }

}