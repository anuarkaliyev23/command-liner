package org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIArgument
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException
import kotlin.reflect.KClass

class ArgumentKeyNotFoundException(
        val key: String,
        val kClass: KClass<*>
): CLIException("Have not found property annotated with @${CLIArgument::class.simpleName} and having key = [$key] in class [$kClass]")