package org.bitbucket.anuarkaliyev23.command.liner.exceptions

open class CLIException(msg: String) : Exception(msg) {
    constructor(): this("")
}