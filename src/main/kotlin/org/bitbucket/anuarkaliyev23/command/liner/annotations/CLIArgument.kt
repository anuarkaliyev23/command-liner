package org.bitbucket.anuarkaliyev23.command.liner.annotations

/**
 * >Property of a class annotated with [CLICommand] must be annotated with this annotation
 * if it represents argument of command.
 * >e.g.
 *
 * `git commit -m "message"`
 * >>`-m "message"` is an argument
 *
 * in `ls -l`
 * >>`-l` is an argument (multiple arguments after single delimiter is not supported by this framework.
 * */
@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class CLIArgument(
        val key: String,
        val helpMessage: String,
        val parserMethodName: String = ""
)