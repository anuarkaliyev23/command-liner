package org.bitbucket.anuarkaliyev23.command.liner.command

import kotlin.reflect.KClass

interface CommandFactoryProvider {
    val injections: Set<Injection<*, *>>

    fun provide(kClass: KClass<*>): CommandFactory
}