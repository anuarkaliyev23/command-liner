package org.bitbucket.anuarkaliyev23.command.liner.annotations

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class CLIInjection (
    val key: String
)