package org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations

import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException
import kotlin.reflect.KClass

class DuplicateKeyException(
        commands: Set<KClass<*>>
) : CLIException("Keys of provided commands contain duplicates. Commands = $commands")