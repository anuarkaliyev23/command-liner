package org.bitbucket.anuarkaliyev23.command.liner.annotations

@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class CLIParameter(
        val key: String
)