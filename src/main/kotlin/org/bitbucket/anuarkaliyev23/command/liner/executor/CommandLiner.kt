package org.bitbucket.anuarkaliyev23.command.liner.executor

import org.bitbucket.anuarkaliyev23.command.liner.annotations.*
/**
 * Implementations are responsible for parsing text into instances of classes annotated with [CLICommand]
 * */
interface CommandLiner : Runnable {
    /**
     * @param line - text line of a command.
     * @return instance of class annotated with [CLICommand]
     * */
    fun parseLine(line: String): Any

    /**
     * @param command - instance of class annotated with [CLICommand]
     * @return result of [command]'s [CLIOnExecute] method call, parsed to String
     * */
    fun executeCommandAsString(command: Any): String
}