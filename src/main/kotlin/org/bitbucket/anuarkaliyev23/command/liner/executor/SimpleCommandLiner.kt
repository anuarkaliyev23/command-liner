package org.bitbucket.anuarkaliyev23.command.liner.executor

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOnExecute
import org.bitbucket.anuarkaliyev23.command.liner.command.CommandFactoryProvider
import org.bitbucket.anuarkaliyev23.command.liner.command.CommandRegistry
import org.bitbucket.anuarkaliyev23.command.liner.extensions.annotatedFunctions
import org.bitbucket.anuarkaliyev23.command.liner.extensions.logger
import java.io.InputStream
import java.io.PrintStream
import java.util.*
import kotlin.collections.HashMap
import kotlin.system.measureTimeMillis

open class SimpleCommandLiner(
        private val commandRegistry: CommandRegistry,
        private val commandFactoryProvider: CommandFactoryProvider,
        private val inputStream: InputStream = System.`in`,
        private val outputStream: PrintStream = System.out,
        private val errorStream: PrintStream = System.err,
        private val delimiter: String = ARGUMENT_DELIMITER,
        private val banner: String = DEFAULT_BANNER
) : CommandLiner {
    private val scanner = Scanner(inputStream)

    private fun tokenize(line: String): List<String>  {
        val result = line.split(delimiter).map { it.trim() }
        this.logger().debug("Tokenized {$line} = {$result}")
        return result
    }

    private fun commandKeyOf(line: String): String {
        val first = tokenize(line).first()
        this.logger().debug("Parsed {$line} command as {$first}")
        return first
    }

    private fun argKeyToValueOf(line: String): Map<String, String> {
        val tokens = tokenize(line)
        if (tokens.size == 1)
            return emptyMap()

        val map = HashMap<String, String>()

        for (i in 1 until tokens.size) {
            val scanner = Scanner(tokens[i])
            val key = scanner.next()
            var argValue = ""
            while (scanner.hasNext()) {
                argValue += scanner.next()
                argValue += " "
            }

            map[key] = argValue
        }

        this.logger().debug("Parsed {$line} into args {$map}")
        return map
    }

    override fun parseLine(line: String): Any {
        val commandKey = commandKeyOf(line)
        val command = commandRegistry.commandOf(commandKey)
        val args = argKeyToValueOf(line)

        val instance = commandFactoryProvider.provide(command).instantiateAndInject(command, args)
        this.logger().debug("Parsed {$line} as {$instance}")
        return instance
    }

    override fun executeCommandAsString(command: Any): String {
        val executorMethod = command::class.annotatedFunctions<CLIOnExecute>().first()
        this.logger().debug("Executor of {$command} is determined to be {$executorMethod}")
        val result = executorMethod.call(command)
        this.logger().debug("Result of a method call is {$result}")
        return result.toString()
    }

    override fun run() {
        print("$banner>")
        val line = scanner.nextLine()
        this.logger().debug("Scanned line = {$line}")
        val command = parseLine(line)
        var result = ""
        val time = measureTimeMillis {
            result = executeCommandAsString(command)
        }
        this.logger().debug("Executed command in {$time} millis")
        println(result)
    }

    companion object {
        const val ARGUMENT_DELIMITER = "--"
        const val DEFAULT_BANNER = "command-liner>"
    }

}