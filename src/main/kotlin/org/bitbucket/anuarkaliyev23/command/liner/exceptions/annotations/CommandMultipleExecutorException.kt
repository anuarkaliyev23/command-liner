package org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOnExecute
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException
import kotlin.reflect.KClass

class CommandMultipleExecutorException(
        val kClass: KClass<*>
) : CLIException("Multiple @${CLIOnExecute::class.simpleName} annotation found on methods in class [$kClass]")