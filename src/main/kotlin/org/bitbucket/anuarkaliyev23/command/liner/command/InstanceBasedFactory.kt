package org.bitbucket.anuarkaliyev23.command.liner.command

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIArgument
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations.ArgumentKeyNotFoundException
import org.bitbucket.anuarkaliyev23.command.liner.extensions.annotatedProperties
import org.bitbucket.anuarkaliyev23.command.liner.extensions.simpleName
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty
import kotlin.reflect.full.*

/**
 * [CommandFactory] based in Kotlin's [KClass.createInstance] method.
 * */
class InstanceBasedFactory(
        override val injections: Set<Injection<*, *>> = emptySet()
) : CommandFactory {
    override fun <T : Any> instantiate(command: KClass<T>, args: Map<String, String>): T {
        val instance = command.createInstance()
        checkMandatoryProperties(command, args)

        args.forEach { (key, value) ->
            val property = propertyByKey(command, key)
            val parserMethodName = property.findAnnotation<CLIArgument>()!!.parserMethodName

            val parsedValue = if (parserMethodName.isNotEmpty()) {
                val memberParserMethod = command.memberFunctions.find { it.simpleName() == parserMethodName }
                val companionParserMethod = command.companionObject?.memberFunctions?.find { it.simpleName() == parserMethodName }
                val parserMethod = memberParserMethod ?: companionParserMethod ?: throw CLIException("Parser method not found")
                parserMethod.call(instance, value)
            }
            else value

            if (property is KMutableProperty<*>) {
                property.setter.call(instance, parsedValue)
            }
        }
        return instance
    }

    private fun propertyByKey(kClass: KClass<*>, argKey: String): KProperty<*> {
        val annotatedProperties = kClass.annotatedProperties<CLIArgument>()
        annotatedProperties.forEach { property ->
            val argumentAnnotation = property.findAnnotation<CLIArgument>()!!
            if (argumentAnnotation.key == argKey)
                return property
        }
        throw ArgumentKeyNotFoundException(argKey, kClass)
    }


}