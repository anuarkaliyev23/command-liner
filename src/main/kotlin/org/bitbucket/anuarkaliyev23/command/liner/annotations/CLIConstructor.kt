package org.bitbucket.anuarkaliyev23.command.liner.annotations

@Target(AnnotationTarget.CONSTRUCTOR)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class CLIConstructor