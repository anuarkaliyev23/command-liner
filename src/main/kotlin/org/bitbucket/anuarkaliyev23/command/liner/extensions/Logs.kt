package org.bitbucket.anuarkaliyev23.command.liner.extensions

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Create [Logger] for given class
 *
 * @receiver [Any]
 * @return [Logger] for given class
 * */
fun Any.logger(): Logger {
    return LoggerFactory.getLogger(this::class.java)
}