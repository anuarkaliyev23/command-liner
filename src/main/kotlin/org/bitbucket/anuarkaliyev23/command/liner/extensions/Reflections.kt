package org.bitbucket.anuarkaliyev23.command.liner.extensions

import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty
import kotlin.reflect.full.*

/**
 * Check if class method or property is annotated with annotation [T]
 *
 * @receiver [KCallable] any member/function
 * @return true if member is annotated, false otherwise
 * */
inline fun <reified T: Annotation> KCallable<*>.isAnnotatedWith(): Boolean {
    return this.annotations.any { it is T }
}

/**
 * Check if class method or property is not annotated with annotation [T]
 *
 * @receiver [KCallable] any member/function
 * @return true if member is annotated, false otherwise
 * */
inline fun <reified T: Annotation> KCallable<*>.isNotAnnotatedWith(): Boolean {
    return this.annotations.none { it is T }
}

/**
 * Get members (properties/functions) annotated with annotation [T]
 *
 * @see KCallable
 * @receiver [KClass]
 * @return Collection of all members annotated with [T]
 * */
inline fun <reified T: Annotation> KClass<*>.annotatedMembers(): Collection<KCallable<*>> {
    return this.members.filter { it.isAnnotatedWith<T>() }
}

/**
 * Get properties annotated with annotation [T]
 *
 * @see KProperty
 * @receiver [KClass]
 * @return [Collection] of all members annotated with [T]
 * */
inline fun <reified T: Annotation> KClass<*>.annotatedProperties(): Collection<KProperty<*>> {
    return this.memberProperties.filter { it.isAnnotatedWith<T>() }
}

inline fun <reified T: Annotation> KClass<*>.annotatedFunctions(): Collection<KFunction<*>> {
    return this.memberFunctions.filter { it.isAnnotatedWith<T>() }
}

/**
 * Get declared members annotated with [T]
 *
 * @see KCallable
 * @see KClass.declaredMembers
 * @receiver [KClass]
 * @return [Collection] of declared members (properties/methods)
 * */
inline fun <reified T: Annotation> KClass<*>.annotatedDeclaredMembers(): Collection<KCallable<*>> {
    return this.declaredMembers.filter { it.isAnnotatedWith<T>() }
}

/**
 * Get declared properties annotated with [T]
 *
 * @see KClass.declaredMemberProperties
 * @receiver [KClass]
 * @return [Collection] of declared [KProperty]
 * */
inline fun <reified T: Annotation> KClass<*>.annotatedDeclaredProperties(): Collection<KProperty<*>> {
    return this.declaredMemberProperties.filter { it.isAnnotatedWith<T>() }
}

/**
 * Get declared methods annotated with [T]
 *
 * @see KClass.declaredMemberFunctions
 * @receiver [KClass]
 * @return [Collection] of declared [KFunction]
 * */
inline fun <reified T: Annotation> KClass<*>.annotatedDeclaredFunctions(): Collection<KFunction<*>> {
    return this.declaredMemberFunctions.filter { it.isAnnotatedWith<T>() }
}

/**
 * Get member name without fully qualified class name
 *
 * @see KCallable
 * @see KClass.simpleName
 * @receiver [KCallable]
 * @return simple name (no fully qualified class name)
 * */
fun KCallable<*>.simpleName(): String {
    return this.name.split(".").last()
}

fun KClass<*>.memberOrCompanionFunction(): Collection<KFunction<*>> {
    val memberFunctions = this.memberFunctions
    val companionMemberFunctions = this.companionObject?.memberFunctions

    return if (companionMemberFunctions == null) {
        memberFunctions
    } else memberFunctions.plus(companionMemberFunctions)
}