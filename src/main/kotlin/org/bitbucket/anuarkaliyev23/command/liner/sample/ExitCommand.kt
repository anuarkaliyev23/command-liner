package org.bitbucket.anuarkaliyev23.command.liner.sample

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLICommand
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOnExecute
import org.bitbucket.anuarkaliyev23.command.liner.sample.ExitCommand.Companion.COMMAND_KEY
import kotlin.system.exitProcess

@CLICommand(key = COMMAND_KEY, helpMessage = "Exit from program")
class ExitCommand {
    @CLIOnExecute
    fun execute() {
        exitProcess(1)
    }
    companion object {
        const val COMMAND_KEY = "exit"
    }
}