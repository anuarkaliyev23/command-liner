package org.bitbucket.anuarkaliyev23.command.liner.exceptions.annotations

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLICommand
import kotlin.reflect.KClass

class ClassNotAnnotatedAsCommandException(kClass: KClass<*>) : ClassNotAnnotatedException(kClass, CLICommand::class)