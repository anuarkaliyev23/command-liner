package org.bitbucket.anuarkaliyev23.command.liner.command

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIInjection
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.CLIException
import org.bitbucket.anuarkaliyev23.command.liner.extensions.isNotAnnotatedWith
import kotlin.reflect.KMutableProperty1

class SimpleInjection<T, R>(
        override val field: KMutableProperty1<T, R>,
        override val fieldValue: R
) : Injection<T, R> {
    init {
        if (field.isNotAnnotatedWith<CLIInjection>())
            throw CLIException("field [$field] is not annotated with @[${CLIInjection::class.simpleName}]")
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SimpleInjection<*, *>

        if (field != other.field) return false
        if (fieldValue != other.fieldValue) return false

        return true
    }

    override fun hashCode(): Int {
        var result = field.hashCode()
        result = 31 * result + (fieldValue?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "SimpleInjection(field=$field, fieldValue=$fieldValue)"
    }


}