package org.bitbucket.anuarkaliyev23.command.liner.command

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIConstructor
import org.bitbucket.anuarkaliyev23.command.liner.extensions.isAnnotatedWith
import org.bitbucket.anuarkaliyev23.command.liner.extensions.logger
import kotlin.reflect.KClass

class AnnotationBasedCommandFactoryProvider(
        override val injections: Set<Injection<*, *>> = emptySet()
) : CommandFactoryProvider {
    init {
        this.logger().debug("Initialized ${this::class.simpleName} with injections = {${injections}}")
    }
    override fun provide(kClass: KClass<*>): CommandFactory {
        return when {
            containsAnnotatedConstructor(kClass) -> {
                this.logger().debug("Providing {$kClass} with CommandFactory {${ConstructorBasedFactory(injections)}}")
                ConstructorBasedFactory(injections)
            }
            else -> {
                this.logger().debug("Providing {$kClass} with CommandFactory {${InstanceBasedFactory(injections)}}")
                InstanceBasedFactory(injections)
            }
        }
    }

    private fun containsAnnotatedConstructor(kClass: KClass<*>): Boolean {
        val constructor = kClass.constructors.firstOrNull { it.isAnnotatedWith<CLIConstructor>() }
        return if (constructor != null) {
            this.logger().debug("Found constructor annotation in {$kClass}")
            true
        } else {
            this.logger().debug("Did not find constructor annotation in {$kClass}")
            false
        }
    }
}