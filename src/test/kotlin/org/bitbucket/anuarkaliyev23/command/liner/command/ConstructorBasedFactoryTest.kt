package org.bitbucket.anuarkaliyev23.command.liner.command

import org.bitbucket.anuarkaliyev23.command.liner.sample.HelpCommand
import org.bitbucket.anuarkaliyev23.command.liner.sample.ImmutableGreeterCommand
import org.bitbucket.anuarkaliyev23.command.liner.sample.MutableGreeterCommand
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.time.LocalDate

internal class ConstructorBasedFactoryTest {

    @Test
    fun instantiate() {
        val instance1 = ConstructorBasedFactory().instantiate(ImmutableGreeterCommand::class, mapOf(
                "fn" to "John",
                "ln" to "Doe",
                "bd" to "2000-01-01"
        ))
        assertEquals(ImmutableGreeterCommand("John", true, "Doe", LocalDate.of(2000, 1, 1)), instance1)

        val instance2 = ConstructorBasedFactory().instantiate(ImmutableGreeterCommand::class, mapOf(
                "fn" to "John"
        ))
        assertEquals(ImmutableGreeterCommand("John"), instance2)

        val instance3 = ConstructorBasedFactory().instantiate(ImmutableGreeterCommand::class, mapOf(
                "fn" to "John",
                "ln" to "Doe"
        ))
        assertEquals(ImmutableGreeterCommand("John", true, "Doe"), instance3)

        val instance4 = ConstructorBasedFactory().instantiate(ImmutableGreeterCommand::class, mapOf(
                "fn" to "John",
                "bd" to "2000-01-01"
        ))
        assertEquals(ImmutableGreeterCommand("John", birthday = LocalDate.of(2000, 1, 1)), instance4)
    }

    @Test
    fun inject() {
        val factory = ConstructorBasedFactory(setOf(SimpleInjection(HelpCommand::registry, ImmutableCommandRegistry(setOf(ImmutableGreeterCommand::class, MutableGreeterCommand::class, HelpCommand::class)))))
        val helpCommand = HelpCommand()
        factory.inject(helpCommand, HelpCommand::class)
        println(helpCommand)
        println(helpCommand.execute())
    }
}