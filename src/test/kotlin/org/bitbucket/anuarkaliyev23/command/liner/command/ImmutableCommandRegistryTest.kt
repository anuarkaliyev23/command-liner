package org.bitbucket.anuarkaliyev23.command.liner.command

import org.bitbucket.anuarkaliyev23.command.liner.command.CommandRegistry
import org.bitbucket.anuarkaliyev23.command.liner.command.ImmutableCommandRegistry
import org.bitbucket.anuarkaliyev23.command.liner.sample.MutableGreeterCommand
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class ImmutableCommandRegistryTest {
    val registry: CommandRegistry = ImmutableCommandRegistry(setOf(MutableGreeterCommand::class))

    @Test
    fun commandOf() {
        assertTrue { registry.commandOf(MutableGreeterCommand.COMMAND_KEY) == MutableGreeterCommand::class }
    }

    @Test
    fun commands() {
        assertTrue { registry.commands() == setOf(MutableGreeterCommand::class) }
    }
}