package org.bitbucket.anuarkaliyev23.command.liner.sample

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIArgument
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLICommand
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOnExecute
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOptional
import org.bitbucket.anuarkaliyev23.command.liner.sample.MutableGreeterCommand.Companion.COMMAND_KEY
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@CLICommand(key = COMMAND_KEY, helpMessage = "Simple sample command to greet")
class MutableGreeterCommand{
    @property:CLIArgument(key = ARGUMENT_FIRST_NAME_KEY, helpMessage = "Name which must be greeted")
    lateinit var firstName: String

    @property:CLIArgument(key = ARGUMENT_LAST_NAME_KEY, helpMessage = "Last name is optional")
    @property:CLIOptional
    var lastName: String? = null

    @property:CLIArgument(key = ARGUMENT_BIRTHDAY_KEY, helpMessage = "Person's birthday", parserMethodName = "parseLocalDate")
    @property:CLIOptional
    var birthday: LocalDate? = null


    @CLIOnExecute
    fun execute(): String {
        return "Hello, $firstName. Last name = $lastName. Born in $birthday"
    }

    fun parseInt(input: String): Int = input.toInt()

    fun parseLocalDate(input: String): LocalDate {
        val formatter = DateTimeFormatter.ISO_LOCAL_DATE
        return LocalDate.parse(input, formatter)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MutableGreeterCommand

        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (birthday != other.birthday) return false

        return true
    }

    override fun hashCode(): Int {
        var result = firstName.hashCode()
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (birthday?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "GreeterCommand(firstName='$firstName', lastName=$lastName, birthday=$birthday)"
    }


    companion object {
        const val COMMAND_KEY = "greet"
        const val ARGUMENT_FIRST_NAME_KEY = "fn"
        const val ARGUMENT_LAST_NAME_KEY = "ln"
        const val ARGUMENT_BIRTHDAY_KEY = "bd"


    }
}