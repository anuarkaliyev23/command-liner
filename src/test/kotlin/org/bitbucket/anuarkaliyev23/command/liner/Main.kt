package org.bitbucket.anuarkaliyev23.command.liner

import org.bitbucket.anuarkaliyev23.command.liner.command.AnnotationBasedCommandFactoryProvider
import org.bitbucket.anuarkaliyev23.command.liner.command.ImmutableCommandRegistry
import org.bitbucket.anuarkaliyev23.command.liner.command.SimpleInjection
import org.bitbucket.anuarkaliyev23.command.liner.executor.SimpleCommandLiner
import org.bitbucket.anuarkaliyev23.command.liner.sample.ExitCommand
import org.bitbucket.anuarkaliyev23.command.liner.sample.HelpCommand
import org.bitbucket.anuarkaliyev23.command.liner.sample.ImmutableGreeterCommand


fun main() {
    val registry = ImmutableCommandRegistry(setOf(ImmutableGreeterCommand::class, HelpCommand::class, ExitCommand::class))
    val injections = SimpleInjection(HelpCommand::registry, registry)

    val commandLiner = SimpleCommandLiner(registry, AnnotationBasedCommandFactoryProvider(setOf(injections)))
    while (true) {
        try {
            commandLiner.run()
        } catch (e: Exception) {
            e.printStackTrace()
            continue
        }
    }
}