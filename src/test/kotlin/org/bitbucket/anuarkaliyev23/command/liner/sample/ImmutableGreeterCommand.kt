package org.bitbucket.anuarkaliyev23.command.liner.sample

import org.bitbucket.anuarkaliyev23.command.liner.annotations.*
import org.bitbucket.anuarkaliyev23.command.liner.sample.ImmutableGreeterCommand.Companion.COMMAND_KEY
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@CLICommand(key = COMMAND_KEY, helpMessage = "Greeter with immutable parameters")
class ImmutableGreeterCommand @CLIConstructor constructor(
        @property:CLIArgument(key = ARGUMENT_FIRST_NAME_KEY, helpMessage = "First Name")
        @param:CLIParameter(ARGUMENT_FIRST_NAME_KEY)
        val firstName: String,

        @property:CLIArgument(key = "sex", helpMessage = "sex", parserMethodName = "parseSex")
        @param:CLIParameter(key = "sex")
        val sex: Boolean = false,

        @property:CLIOptional
        @property:CLIArgument(key = ARGUMENT_LAST_NAME_KEY, helpMessage = "Last Name")
        @param:CLIParameter(ARGUMENT_LAST_NAME_KEY)
        val lastName: String? = null,



        @property:CLIOptional
        @property:CLIArgument(key = ARGUMENT_BIRTHDAY_KEY, helpMessage = "Birthday", parserMethodName = BIRTHDAY_PARSER_METHOD_NAME)
        @param:CLIParameter(ARGUMENT_BIRTHDAY_KEY)
        val birthday: LocalDate? = null
) {
    @CLIOnExecute
    fun execute(): String {
        return "Hello, ${if (sex) "Mr." else "Mrs."} $firstName. Last name = $lastName. Born in $birthday"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ImmutableGreeterCommand

        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (birthday != other.birthday) return false

        return true
    }

    override fun hashCode(): Int {
        var result = firstName.hashCode()
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (birthday?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "ImmutableGreeterCommand(firstName='$firstName', lastName=$lastName, birthday=$birthday)"
    }


    companion object {
        const val COMMAND_KEY = "imgreet"
        const val ARGUMENT_FIRST_NAME_KEY = "fn"
        const val ARGUMENT_LAST_NAME_KEY = "ln"
        const val ARGUMENT_BIRTHDAY_KEY = "bd"
        const val BIRTHDAY_PARSER_METHOD_NAME = "parseLocalDate"

        fun parseInt(input: String): Int = input.toInt()

        fun parseLocalDate(input: String): LocalDate {
            val formatter = DateTimeFormatter.ISO_LOCAL_DATE
            return LocalDate.parse(input, formatter)
        }

        fun parseSex(input: String): Boolean {
            return input.toLowerCase() == "male"
        }
    }
}