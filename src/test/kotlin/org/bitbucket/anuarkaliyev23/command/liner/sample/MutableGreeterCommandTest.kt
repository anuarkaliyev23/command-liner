package org.bitbucket.anuarkaliyev23.command.liner.sample

import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIArgument
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLICommand
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOnExecute
import org.bitbucket.anuarkaliyev23.command.liner.annotations.CLIOptional
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue


internal class MutableGreeterCommandTest {
    @Test
    fun reflection() {
        assertTrue { MutableGreeterCommand::class.annotations.find { it is CLICommand } != null }
        assertTrue { MutableGreeterCommand::execute.annotations.find { it is CLIOnExecute } != null }
        assertTrue { MutableGreeterCommand::firstName.annotations.find { it is CLIArgument } != null }
        assertTrue { MutableGreeterCommand::lastName.annotations.find { it is CLIArgument } != null }
        assertTrue { MutableGreeterCommand::lastName.annotations.find { it is CLIOptional } != null }
    }
}