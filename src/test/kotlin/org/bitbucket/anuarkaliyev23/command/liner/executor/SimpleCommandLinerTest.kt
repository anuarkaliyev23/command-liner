package org.bitbucket.anuarkaliyev23.command.liner.executor

import org.bitbucket.anuarkaliyev23.command.liner.command.AnnotationBasedCommandFactoryProvider
import org.bitbucket.anuarkaliyev23.command.liner.command.ImmutableCommandRegistry
import org.bitbucket.anuarkaliyev23.command.liner.command.InstanceBasedFactory
import org.bitbucket.anuarkaliyev23.command.liner.exceptions.MandatoryArgumentKeyIsOmittedException
import org.bitbucket.anuarkaliyev23.command.liner.sample.MutableGreeterCommand
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.assertThrows
import java.time.LocalDate
import kotlin.test.assertEquals

internal class SimpleCommandLinerTest {
    companion object {
        lateinit var commandLiner: CommandLiner

        @JvmStatic
        @BeforeAll
        fun initializeCommandLiner() {
            val registry = ImmutableCommandRegistry(setOf(MutableGreeterCommand::class))
            val factory = InstanceBasedFactory()
            commandLiner = SimpleCommandLiner(registry, AnnotationBasedCommandFactoryProvider())
        }
    }

    @Test
    fun tokenize() {
        val command1 = MutableGreeterCommand().apply {
            firstName = "John"
            lastName = "Doe"
            birthday = LocalDate.of(1997, 9, 11)
        }

        val command2 = MutableGreeterCommand().apply {
            firstName = "John"
            lastName = "Doe"
            birthday = null
        }

        val command3 = MutableGreeterCommand().apply {
            firstName = "John"
            lastName = null
            birthday = null
        }

        assertEquals(command1, commandLiner.parseLine("greet --fn John --ln Doe --bd 1997-09-11"))
        assertEquals(command2, commandLiner.parseLine("greet --fn John --ln Doe"))
        assertEquals(command3, commandLiner.parseLine("greet --fn John"))
        assertThrows<MandatoryArgumentKeyIsOmittedException> {  commandLiner.parseLine("greet") }
    }
}