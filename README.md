# Command Liner

## CLI Usage examples
This framework is intended to design CLIs
 
`command --key somevalue --secondKey someOtherValue`

## Code Usage example

There are two main ways of creating CLI Command classes.

+ Using mutable properties 

```
@CLICommand(key = COMMAND_KEY, helpMessage = "Simple sample command to greet")
class MutableGreeterCommand{
    @property:CLIArgument(key = ARGUMENT_FIRST_NAME_KEY, helpMessage = "Name which must be greeted")
    lateinit var firstName: String

    @property:CLIArgument(key = ARGUMENT_LAST_NAME_KEY, helpMessage = "Last name is optional")
    @property:CLIOptional
    var lastName: String? = null

    @property:CLIArgument(key = ARGUMENT_BIRTHDAY_KEY, helpMessage = "Person's birthday", parserMethodName = "parseLocalDate")
    @property:CLIOptional
    var birthday: LocalDate? = null


    @CLIOnExecute
    fun execute(): String {
        return "Hello, $firstName. Last name = $lastName. Born in $birthday"
    }

    fun parseInt(input: String): Int = input.toInt()

    fun parseLocalDate(input: String): LocalDate {
        val formatter = DateTimeFormatter.ISO_LOCAL_DATE
        return LocalDate.parse(input, formatter)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MutableGreeterCommand

        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (birthday != other.birthday) return false

        return true
    }

    override fun hashCode(): Int {
        var result = firstName.hashCode()
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (birthday?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "GreeterCommand(firstName='$firstName', lastName=$lastName, birthday=$birthday)"
    }

    companion object {
        const val COMMAND_KEY = "greet"
        const val ARGUMENT_FIRST_NAME_KEY = "fn"
        const val ARGUMENT_LAST_NAME_KEY = "ln"
        const val ARGUMENT_BIRTHDAY_KEY = "bd"
    }
}
```

+ Using immutable properties and constructor
```
    @CLICommand(key = COMMAND_KEY, helpMessage = "Greeter with immutable parameters")
    class ImmutableGreeterCommand @CLIConstructor constructor(
            @property:CLIArgument(key = ARGUMENT_FIRST_NAME_KEY, helpMessage = "First Name")
            @param:CLIParameter(ARGUMENT_FIRST_NAME_KEY)
            val firstName: String,
    
            @property:CLIOptional
            @property:CLIArgument(key = ARGUMENT_LAST_NAME_KEY, helpMessage = "Last Name")
            @param:CLIParameter(ARGUMENT_LAST_NAME_KEY)
            val lastName: String? = null,
    
            @property:CLIOptional
            @property:CLIArgument(key = ARGUMENT_BIRTHDAY_KEY, helpMessage = "Birthday", parserMethodName = "parseLocalDate")
            @param:CLIParameter(ARGUMENT_BIRTHDAY_KEY)
            val birthday: LocalDate? = null
    ) {
        @CLIOnExecute
        fun execute(): String {
            return "Hello, $firstName. Last name = $lastName. Born in $birthday"
        }
    
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
    
            other as ImmutableGreeterCommand
    
            if (firstName != other.firstName) return false
            if (lastName != other.lastName) return false
            if (birthday != other.birthday) return false
    
            return true
        }
    
        override fun hashCode(): Int {
            var result = firstName.hashCode()
            result = 31 * result + (lastName?.hashCode() ?: 0)
            result = 31 * result + (birthday?.hashCode() ?: 0)
            return result
        }
    
        override fun toString(): String {
            return "ImmutableGreeterCommand(firstName='$firstName', lastName=$lastName, birthday=$birthday)"
        }
    
    
        companion object {
            const val COMMAND_KEY = "imgreet"
            const val ARGUMENT_FIRST_NAME_KEY = "fn"
            const val ARGUMENT_LAST_NAME_KEY = "ln"
            const val ARGUMENT_BIRTHDAY_KEY = "bd"
    
            fun parseInt(input: String): Int = input.toInt()
    
            fun parseLocalDate(input: String): LocalDate {
                val formatter = DateTimeFormatter.ISO_LOCAL_DATE
                return LocalDate.parse(input, formatter)
            }
        }
    }
```